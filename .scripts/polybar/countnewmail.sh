#!/bin/sh
for account in $(mw -l | awk '{print $2}'); do
  count=$(fd -t f . .local/share/mail/"$account"/*/new | wc -l)
  amounts=$amounts$count'|'
done

printf " ${amounts%|}"
